import mongoose from "mongoose"
const Schema = mongoose.Schema
import mongodb from "mongodb"
import mergePlugin from "mongoose-doc-merge"
import { BaseDataInterface } from "@brownpapertickets/surya-gql-data"

export class MongoDB extends BaseDataInterface {
  constructor(container) {
    super(container)

    this.mongoose = container.mongoose || mongoose
    this.mongodb = container.mongodb || mongodb
    //Leave this undefined if we do not provide a mock
    this.connection = container.connection
    this.mongoose.plugin(mergePlugin)
    this.collections = {}
  }

  // 3. Connect to db and attach Schemas ( from Types )
  async connect(connectionString, isProduction) {
    this.log.info(`GraphQL MongoDb Connect uri: ${connectionString}`)

    this.connection = await this.mongoose.createConnection(connectionString, {
      promiseLibrary: Promise,
      autoIndex: !isProduction,
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    })

    this.attachMongoDbSchemas(this.apis, this.typeSpecsMap, this.connection)
    return this
  }

  attachMongoDbSchemas(apiMap, typeMap, connection) {
    const typeName = ""
    Object.keys(apiMap).forEach((modelName) => {
      const api = apiMap[modelName]
      try {
        const { schemaDef, options } = api.toMongooseSchema(typeMap)
        const collectionName = api.getCollectionName()
        const schema = new Schema(schemaDef, options)
        const col = connection.model(collectionName, schema)
        this.collections[collectionName] = col
        this.log.info(`Attached Schema for ${modelName} `)
      } catch (ex) {
        this.log.error(`!!! Error attaching Type: ${modelName} to MongoDB `)
      }
    })
  }

  async disconnect() {
    if (this.connection) {
      await this.connection.close()
    }
  }

  newObjectId(s) {
    // If s is undefined, then a new ObjectID is created, else s is assumed to be a parsable ObjectID
    return new this.mongodb.ObjectID(s)
  }

  // Transaction related methods ========================

  async startTransaction() {
    const connection = this.connection
    let session = await connection.startSession()
    session.startTransaction()
    return session
  }

  async commitTransaction(session) {
    await session.commitTransaction()
    return true
  }

  async abortTransaction(session) {
    this.log.error(`mongodb abortTransaction! `)
    await session.abortTransaction()
    session.endSession()
    return true
  }
}
