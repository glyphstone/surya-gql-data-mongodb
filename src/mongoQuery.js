import { QueryParser } from "@brownpapertickets/surya-gql-data"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

/**
 * Transcoder from QueryParser syntax tree formats to
 * MongoDB filter and sort object formats.
 */
export class MongoQuery {
  constructor(container, type, typeMap) {
    this.log = container.log
    this.type = type // the root Type object
    this.typeMap = typeMap // map object of all the typeSpecs for all objects incase of potential nested reference.
    this.typeFields = this.genTypeFields("", this.type.typeSpec().fields)
    this.queryParser = new QueryParser()
  }

  /**
   * create an expanded list of all field names as known to the MongoDB database.
   * accounting for objectRef (id) fields
   * and nested object dot-notation references.
   */
  genTypeFields(prefix, fieldSpecs) {
    let typeFields = {}
    const fieldNames = Object.keys(fieldSpecs)
    fieldNames.forEach((fieldName) => {
      const fieldSpec = fieldSpecs[fieldName]
      let useFieldName = prefix.length ? prefix + "." + fieldName : fieldName
      switch (fieldSpec.type) {
        case ScalarTypes.Object:
          const subObjectType = fieldSpec.objectType
          const subSpec = this.typeMap[subObjectType]
          if (subSpec) {
            const subTypeFields = this.genTypeFields(
              useFieldName,
              subSpec.fields
            )
            typeFields = Object.assign(typeFields, subTypeFields)
          } else {
            throw new Error(`Type not found: ${subObjectType}`)
          }
          break
        case ScalarTypes.ObjectRef:
          const refFieldName = fieldSpec.refField
          useFieldName = prefix.length
            ? prefix + "." + refFieldName
            : refFieldName
          typeFields[useFieldName] = fieldSpec
          // consider object.id alias addition
          break
        default:
          typeFields[useFieldName] = fieldSpec
          break
      }
    })

    // this.log.info(`typeFields ${JSON.stringify(typeFields, null, 2)}`)
    return typeFields
  }

  queryFilter(filter) {
    if (filter.trim().length > 0) {
      let root
      try {
        root = this.queryParser.parseFilter(filter)
      } catch (ex) {
        this.log.error(`Error parsing filter string: '${filter}'`)
        throw new Error(`Invalid filter: ${ex.message}`)
      }
      return this.transFilterNode(root)
    }
    return {}
  }

  transFilterNode(node) {
    switch (node.type) {
      case "andClause":
        return this.transFilterAnd(node)
        break
      case "orClause":
        return this.transFilterOr(node)
        break
      case "notExpression":
        return this.transFilterNot(node)
        break
      case "equalPredicate":
      case "notEqualPredicate":
      case "greaterEqualPredicate":
      case "greaterPredicate":
      case "lessEqualPredicate":
      case "lessPredicate":
      case "matchesPredicate":
      case "notMatchesPredicate":
        return this.transFilterPredicate(node)
        break
      case "existsPredicate":
      case "notExistsPredicate":
        return this.transFilterExistsPredicate(node)
        break
      case "inPredicate":
      case "notInPredicate":
        return this.transFilterInPredicate(node)
        break
      case "stringLiteral":
        throw new Error(`String literal not expected as terminal node`)
        break
      case "number":
        throw new Error(`Numeric literal not expected as terminal node`)
        break
      case "trueLiteral":
      case "falseLiteral":
        throw new Error(
          "boolean (true/false) literal not expected as terminal node"
        )
      default:
        throw new Error(`node type not recognized: ${node.type}`)
    }
  }

  transFilterAnd(node) {
    if (node.children.length < 2) {
      throw new Error(`AND expects two children`)
    }
    const arg1 = this.transFilterNode(node.children[0])
    const arg2 = this.transFilterNode(node.children[1])
    return { $and: [arg1, arg2] }
  }

  transFilterOr(node) {
    if (node.children.length < 2) {
      throw new Error(`OR expects two children`)
    }
    const arg1 = this.transFilterNode(node.children[0])
    const arg2 = this.transFilterNode(node.children[1])
    return { $or: [arg1, arg2] }
  }

  transFilterNot(node) {
    throw new Error("Not(), Not Currently Supported. Stay tuned!")
    return {}
  }

  transFilterPredicate(node) {
    let pred = {}
    const { fieldName, fieldParams } = this.getFieldReference(node.children)
    const literals = this.getLiterals(node.children)
    const rvalue = literals[0]
    const fieldSpec = this.getFieldSpec(fieldName)
    const dbFieldName = this.getDbFieldName(fieldName, fieldSpec)
    switch (node.type) {
      case "equalPredicate":
        pred[dbFieldName] = { $eq: rvalue }
        break
      case "notEqualPredicate":
        pred[dbFieldName] = { $ne: rvalue }
        break
      case "greaterEqualPredicate":
        pred[dbFieldName] = { $gte: rvalue }
        break
      case "greaterPredicate":
        pred[dbFieldName] = { $gt: rvalue }
        break
      case "lessEqualPredicate":
        pred[dbFieldName] = { $lte: rvalue }
        break
      case "lessPredicate":
        pred[dbFieldName] = { $lt: rvalue }
        break
      case "matchesPredicate":
        pred[dbFieldName] = { $regex: rvalue, $options: "i" }
        break
    }
    return pred
  }

  getFieldSpec(fieldName) {
    if (this.typeFields.hasOwnProperty(fieldName)) {
      return this.typeFields[fieldName]
    } else {
      throw new Error(
        `Query Parser error: Field: '${fieldName}' does not exist`
      )
    }
  }

  getDbFieldName(fieldName, fieldSpec) {
    let dbFieldName = fieldName
    if (fieldSpec.key) {
      dbFieldName = "_id"
    }
    if (fieldSpec.asName) {
      let prefix = ""
      const trimTo = fieldName.lastIndexOf(".")
      if (trimTo >= 0) {
        prefix = fieldName.substring(0, trimTo + 1)
      }
      dbFieldName = prefix + fieldSpec.asName
    }

    return dbFieldName
  }

  transFilterExistsPredicate(node) {
    let pred = {}
    const { fieldName, fieldParams } = this.getFieldReference(node.children)
    const fieldSpec = this.getFieldSpec(fieldName)
    const dbFieldName = this.getDbFieldName(fieldName, fieldSpec)
    switch (node.type) {
      case "existsPredicate":
        pred[dbFieldName] = { $exists: true }
        break
      case "notExistsPredicate":
        pred[dbFieldName] = { $exists: false }
        break
    }
    return pred
  }

  transFilterInPredicate(node) {
    let pred = {}
    const { fieldName, fieldParams } = this.getFieldReference(node.children)
    const fieldSpec = this.getFieldSpec(fieldName)
    const dbFieldName = this.getDbFieldName(fieldName, fieldSpec)
    const literals = this.getLiterals(node.children)
    switch (node.type) {
      case "inPredicate":
        pred[dbFieldName] = { $in: literals }
        break
      case "notInPredicate":
        pred[dbFieldName] = { $nin: literals }
        break
    }
    return pred
  }

  getFieldReference(args) {
    const fields = args.filter((fitem) => fitem.type == "field")

    if (fields.length == 0) {
      console.log(`Error finding field: ${JSON.stringify(args, null, 2)}`)
      throw new Error("field not found")
    }

    const field = fields[0]
    let fieldParams = []
    const identifiers = field.children.filter(
      (item) => item.type == "identifier"
    )

    if (identifiers.length == 0) {
      throw new Error("Field identifier not found")
    }

    const fieldName = identifiers[0].value

    const paramList = field.children.filter((item) => item.type == "paramList")
    if (paramList.length) {
      fieldParams = this.getLiterals(paramList[0].children)
    }

    return { fieldName, fieldParams }
  }

  getLiterals(args) {
    const literals = args
      .filter(
        (item) =>
          item.type == "stringLiteral" ||
          item.type == "number" ||
          item.type == "trueLiteral" ||
          item.type == "falseLiteral"
      )
      .map((item) => item.value)
    if (literals.length == 0) {
      throw new Error("literal not found")
    }
    return literals
  }

  getAscDesc(args) {
    const literals = args.filter(
      (item) => item.type == "asc" || item.type == "desc"
    )
    if (literals.length == 0) {
      return 1
    }
    return literals[0].value == "desc" ? -1 : 1
  }

  querySort(sort) {
    if (sort.trim().length > 0) {
      let root
      try {
        root = this.queryParser.parseSort(sort)
        if (root.type != "sortSpecList") {
          throw new Error(`Invalid sort`)
        }
      } catch (ex) {
        this.log.error(`Error parsing sort string: '${sort}'`)
        throw new Error(`Invalid sort: ${ex.message}`)
      }

      const sortList = root.children
      return this.transSortList(sortList)
    }
    return {}
  }

  transSortList(sortList) {
    let mongoSort = {}
    sortList.forEach((node) => {
      const { field, dir } = this.transSort(node)
      mongoSort[field] = dir
    })
    return mongoSort
  }

  transSort(node) {
    const { fieldName, fieldParams } = this.getFieldReference(node.children)
    const fieldSpec = this.getFieldSpec(fieldName)
    const dbFieldName = this.getDbFieldName(fieldName, fieldSpec)
    const dir = this.getAscDesc(node.children)
    return { field: dbFieldName, dir }
  }
}
