export class MockLog {
  info(message) {
    console.log(`Info:${message}`)
  }
  error(message) {
    console.log(`Error:${message}`)
  }
  warn(message) {
    console.log(`Warn:${message}`)
  }
}
