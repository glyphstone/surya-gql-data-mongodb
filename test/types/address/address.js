/* generic non-instantiable types for general use
 */

import { BaseType } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class Address extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Address",
      extends: null,
      instantiable: true,
      fields: {
        address1: {
          type: ScalarTypes.String,
        },
        address2: {
          type: ScalarTypes.String,
        },
        country: {
          type: ScalarTypes.Enum,
          values: ["US", "CA"],
          defaultValue: "US",
        },
        postalCode: {
          type: ScalarTypes.String,
          required: false,
          matches: "[0-9]{5}(s*-s*[0-9]{4})?",
          asName: "postal_code",
          unique: true,
        },
        stateProvince: {
          type: ScalarTypes.String,
          asName: "state",
        },
        city: {
          type: ScalarTypes.String,
        },
        addressType: {
          type: ScalarTypes.String,
        },
        isDefaultAddress: {
          type: ScalarTypes.Boolean,
          asName: "default_address",
          defaultValue: false,
        },
        dateCreated: {
          type: ScalarTypes.DateTime,
          asName: "date_created",
          defaultValue: "$NOW",
        },
      },
    }
  }
}
