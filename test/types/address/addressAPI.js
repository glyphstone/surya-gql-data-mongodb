import { BaseMongoAPI } from "../../../src/baseMongoAPI"

export class AddressAPI extends BaseMongoAPI {
  constructor(container, type, typeMap, config) {
    super(container, type, typeMap, config)
    this.log.info(
      `AddressAPI create type: ${this.getTypeName()} config:${JSON.stringify(
        config
      )}`
    )
  }
}
