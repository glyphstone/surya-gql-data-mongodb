import { MongoDB } from "../src/mongoDB"
import { MockLog } from "./mockLog"

let container
beforeAll(() => {
  container = { log: new MockLog() }
})

describe("mongoDB Tests", () => {
  test("Test instantiate MongoDB", async () => {
    const mongoDB = new MongoDB(container)
    const apis = mongoDB.getAllAPIs()
    const typeSpecs = mongoDB.getTypeSpecs()
    expect(apis).toEqual({})
    expect(typeSpecs).toEqual({})
  })

  test("Test connect", async () => {
    const mongoDB = new MongoDB(container)
    const mongoConnection = "mongodb://localhost/bpt"
    const it = await mongoDB.connect(mongoConnection, false)
    console.log("connected")
  })
})
