import { MongoQuery } from "../src/mongoQuery"
import { MockLog } from "./mockLog"
import { Address } from "./types/address"
import { Venue } from "./types/venue"

let container
let venueMongoQuery
beforeAll(() => {
  container = { log: new MockLog() }

  let typeMap = {
    Venue: new Venue().typeSpec(),
    Address: new Address().typeSpec(),
  }
  venueMongoQuery = new MongoQuery(container, new Venue(), typeMap)
})

describe("mongoQuery Venue Parser Tests", () => {
  describe("basic predicate op coverage", () => {
    test("test Venue Mongo id = value", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "id = '5dc095a0be9d1f1ac3366382'"
      )
      console.log(mongoFilter)
      expect(mongoFilter).toEqual({ _id: { $eq: "5dc095a0be9d1f1ac3366382" } })
    })

    test("test Venue Mongo filter = production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "name = 'BrownPaperTickets'"
      )
      //console.log(mongoFilter)
      expect(mongoFilter).toEqual({ name: { $eq: "BrownPaperTickets" } })
    })

    test("test Venue Mongo filter != production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "name != 'BrownPaperTickets'"
      )
      //console.log(mongoFilter)
      expect(mongoFilter).toEqual({ name: { $ne: "BrownPaperTickets" } })
    })

    test("test Venue Mongo filter > production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "name > 'BrownPaperTickets'"
      )
      expect(mongoFilter).toEqual({ name: { $gt: "BrownPaperTickets" } })
    })

    test("test Venue Mongo filter >= production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "name >= 'BrownPaperTickets'"
      )
      expect(mongoFilter).toEqual({ name: { $gte: "BrownPaperTickets" } })
    })

    test("test Venue Mongo filter < production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "name < 'BrownPaperTickets'"
      )
      expect(mongoFilter).toEqual({ name: { $lt: "BrownPaperTickets" } })
    })

    test("test Venue Mongo filter <= production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "name <= 'BrownPaperTickets'"
      )
      expect(mongoFilter).toEqual({ name: { $lte: "BrownPaperTickets" } })
    })

    test("test Venue Mongo filter matches production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter("name MATCHES 'Brown'")
      expect(mongoFilter).toEqual({ name: { $regex: "Brown", $options: "i" } })
    })

    test("test Venue Mongo filter asName venueActive = true", async () => {
      const mongoFilter = venueMongoQuery.queryFilter("venueActive = true")
      expect(mongoFilter).toEqual({ active: { $eq: true } })
    })

    test("test Venue Mongo filter subname = value", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "address.city = 'Seattle'"
      )
      expect(mongoFilter).toEqual({ "address.city": { $eq: "Seattle" } })
    })

    test("test Venue Mongo filter 'subname = value' with asName", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "address.postalCode = '98109'"
      )
      expect(mongoFilter).toEqual({ "address.postal_code": { $eq: "98109" } })
    })

    test("test Venue Mongo filter in production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "name in ( 'Brown', 'Paper', 'Tickets' )"
      )
      //console.log(JSON.stringify(mongoFilter))
      expect(mongoFilter).toEqual({
        name: { $in: ["Brown", "Paper", "Tickets"] },
      })
    })

    test("test Venue Mongo filter !IN production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "name !IN ( 'Brown', 'Paper', 'Tickets' )"
      )
      //console.log(JSON.stringify(mongoFilter))
      expect(mongoFilter).toEqual({
        name: { $nin: ["Brown", "Paper", "Tickets"] },
      })
    })

    test("test Venue Mongo filter exists production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter("name EXISTS ")
      //console.log(JSON.stringify(mongoFilter))
      expect(mongoFilter).toEqual({ name: { $exists: true } })
    })

    test("test Venue Mongo filter !EXISTS production", async () => {
      const mongoFilter = venueMongoQuery.queryFilter("name !exists ")
      expect(mongoFilter).toEqual({ name: { $exists: false } })
    })
  })

  test("test Venue address sub field", async () => {
    const mongoFilter = venueMongoQuery.queryFilter("address.city = 'Seattle' ")
    // console.log(mongoFilter)
    expect(mongoFilter).toEqual({ "address.city": { $eq: "Seattle" } })
  })

  describe("Complex and fail scenarios for Venue Parser", () => {
    test("test Venue Mongo filter complex and/or", async () => {
      const mongoFilter = venueMongoQuery.queryFilter(
        "name matches 'Brown' AND address.city = 'Seattle' OR name matches 'tickets' AND address.city = 'London' "
      )
      // console.log(JSON.stringify(mongoFilter))
      expect(mongoFilter).toEqual({
        $or: [
          {
            $and: [
              { name: { $regex: "Brown", $options: "i" } },
              { "address.city": { $eq: "Seattle" } },
            ],
          },
          {
            $and: [
              { name: { $regex: "tickets", $options: "i" } },
              { "address.city": { $eq: "London" } },
            ],
          },
        ],
      })
    })

    test("test Venue Mongo filter fail on bad field name", async () => {
      try {
        const mongoFilter = venueMongoQuery.queryFilter(
          "doesNotExists = 'isBogus' "
        )
        expect(true).toBe(false)
      } catch (ex) {
        const msg = ex.message
        console.log(`Take this branch: ${msg}`)
        expect(msg).toEqual(
          "Query Parser error: Field: 'doesNotExists' does not exist"
        )
      }
    })
  })
})
