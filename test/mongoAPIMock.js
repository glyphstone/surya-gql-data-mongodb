export class MongoAPIMock {
  constructor(container, type) {
    this.container = container
    this.log = container.log
    this.db = container.db
    this.type = type
  }
  async query(offset = 0, limit = DEFAULT_LIMIT, filter = "", sort = "") {
    result = {}
    return result
  }

  async get({ id }) {
    return {}
  }

  async create(obj) {
    return obj
  }

  async update(id, obj) {
    return obj
  }

  async delete(id) {
    return {}
  }
}
