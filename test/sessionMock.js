export class SessionMock {
  constructor(container) {
    this.container = container
  }

  async get(authToken) {
    return {
      authToken,
      user: { _id: "5c33f888de96d370761e4132", administrator: true },
    }
  }

  async set(authToken, session) {
    console.log(`Mock Session set authToken: ${authToken}  session: ${session}`)
  }

  async clear() {}
}
